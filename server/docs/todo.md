# Legenda

X  - Implementado
R  - Possuí rota definida
[] - Não Implementado

## Usuário <-> Cadastro

[X-R] Criar Usuário (authSignin)
[X-R] Fazer login (authLogin)
[X-R] Atualizar profile (profilePatch)
[X-R] Deletar usuario (profileDelete)
[X-R] Recuperar informacoes do meu profile (profileGet)
[X-R] Recuperar informações de configuração do usuário (profileSettingGet)
[X-R] Alterar configurações do usuário (profileSettingPatch)
[] Mudar e-mail
[] Enviar email quando novo cadastro for feito
[] Enviar email para usuário em um pedido de alterar email
[] Enviar email para o usuário quando esse esqueceu a senha
[] Enviar email para o usuário quando esse quiser deletar seu perfil
[] Confirmar novo usuario por email
[] Confirmar mudanca de email por email
[] Confirmar alterar senha (esqueci senha)
[] Confirmar deletar usuario por email

## Usuário <-> Mensagens

[X] Recuperar mensagens não deletadas feitas pelo usuário (profileMessageSentGetAll)
[X] Recuperar mensagens enviadas para este usuário (profileMessageRecvAll)
[X] Marcar mensagens recebidas como lida (profileMessageMarkAsReadPatch)

## Usuário <-> Review

[] Recuperar lista de reviews feitos
[] Recuperar lista de reviews recebidos

## Usuário <-> Produto

[X-R] Cadastrar novo produto (productCreate)
[X-R] Editar produto (prodcutPatch)
[X-R] Remover produto (productDelete)
[X-R] Listar todos os produtos (productGetAll)
[X-R] Listar todos os produtos do usuário (productsGetMine)
[X-R] Recolher info de um produto específico (productGet)
[X] Visualizar grupos de um produto (productGetAllGroups)
[X] Visualizar coletores de um produto (productGetAllCollectors)
[X] Enviar mensagem para todos os coletores desse produto (productSendBroadcastMessage)

## Usuário <-> Coleta

[X] Se cadastrar na coleta (productPickUpSignUp)
[X] Cancelar cadastro na coleta (productPickUpCancel)

## Usuário <-> Usuário

[X] Escrever nova mensagem (profileMessageSendPatch)
[X] Receber informacoes de um usuario (profileGetUser)
[] Fazer review do usuário

## Usuário <-> Grupo (permissões de usuário)

[X-R] Filtrar Grupos (groupFilter)
[X-R] Cadastrar novo grupo (groupCreate)
[X-R] Listar todos os usuarios de um grupo (groupMembersGetAll)
[X-R] Listar todos os grupos (groupGetAll)
[X-R] Entrar em um grupo público (groupMembersJoinPatch)
[X-R] Entrar em um grupo privado (groupMembersJoinPatch)
[X-R] Sair de um grupo (groupMembersLeavePatch)
[X-R] Coletar informações de um grupo (groupGet)
[X-R] Listar protudos cadastrados em um grupo (groupProductsGetAll)
[X-R] Listar meus produtos cadastrados em um grupo (groupProductsGetMine)
[X-R] Cadastrar meus produtos em um grupo (groupProductsAddPatch)
[X-R] Remover um produto meu de um grupo (groupProductsRemove)
[X] Listar todos os grupos que um usuário participa (groupGetAllParticipate)
[X] Listar mensagens de um grupo (groupMessagesGetAll)
[X] Enviar mensagem para um grupo (groupMessagesSendPatch)
[] Apagar minhas mensagens de um grupo
[] Editar minhas mensagens de um grupo

## Usuário <-> Grupo (permissões de admin)

[X-R] Listar todos os meus grupos (groupGetMine)
[X-R] Editar grupo (groupPatch)
[X-R] Remover grupo (groupDelete)
[X-R] Bloquear um usuario (groupMembersBlockPatch)
[X-R] Desbloquear um usuario (groupMembersBlockPatch)
[X-R] Admin pode quicar um usuario (groupMembersKickPatch)
[X] Listar usuarios bloqueados de um grupo (groupMembersBlockedGetAll)
[X] Listar produtos bloqueados de um grupo (groupProductsBlockedGetAll)
[X] Remover produto nao meu de um grupo (groupProductsRemove)
[X] Bloquear um produto (groupProductsBlockPatch)
[X] Desbloquear um produto (groupProductsBlockPatch)
[] Apagar mensagens nao minhas de um grupo
