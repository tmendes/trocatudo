'use strict';

require('./config/config');
require('./db/mongoose');

const express = require('express');
const passport = require('passport');
const bodyParser = require('body-parser');

const morgan = require('morgan');

require('./services/passport-setup');

const authRouters = require('./routers/auth-routers');
const profileRouters = require('./routers/profile-routers');
const productRouters = require('./routers/product-routers');
const groupRouters = require('./routers/group-routers');
const messageRouters = require('./routers/message-routers');
const reviewRouters = require('./routers/review-routers');

const app = express();
const port = process.env.PORT;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(morgan('dev'));

app.use(passport.initialize());

/* Setup to prevent CORS errors */
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTION') {
        res.header(
            'Access-Control-Allow-Methods',
            'GET, PUT, POST, PATCH, DELETE'
        );
        return res.status(200).json({});
    }
    return next();
});

/* Routers */
app.use('/auth', authRouters);
app.use('/user', profileRouters);
app.use('/share', productRouters);
app.use('/basket', groupRouters);
app.use('/messages', messageRouters);
app.use('/review', reviewRouters);

app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

app.listen(port, () => {
    console.log(`Listening for requests on http://localhost:${port}`);
});

module.exports = { app };
