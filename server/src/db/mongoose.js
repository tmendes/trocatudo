'use strict';

const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

/* Open a connection to our MongoDB Database */
mongoose.connect(process.env.MONGODB_URI).then(() => {
    console.log('Connected to the mongoDB');
}).catch((e) => {
    console.error(`Unable to connect to ${process.env.MONGODB_URI}. Is it
    running?`);
    process.exit(1);
});

module.exports = mongoose;
