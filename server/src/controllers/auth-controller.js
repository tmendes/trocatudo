const jwt = require('jsonwebtoken');

const { User } = require('./../models/user-model');

const keys = require('./../config/keys-config');

const jrh = require('./../utils/jsonResponseHandler');

const tokenExpiresIn = '1d';

const msgs = {
    catch: { msg: 'ops' },
    invalid_username: { msg: 'invalid_username' },
    invalid_password: { msg: 'invalid_password' },
    user_already_exists: { msg: 'user_already_exists' },
    cant_create_new_user: { msg: 'cant_create_new_user' },
    deleted: { msg: 'user_deleted' }
};

const handleCatch = (e) => {
    console.error(e);
    return jrh.fail(msgs.catch);
};

const authLogin = async (usernameOrEmail, password) => {
    const conditions = {
        $or: [{
            username: usernameOrEmail,
            'email.address': usernameOrEmail
        }]
    };
    try {
        const user = await User.findOne(conditions);
        if (!user) {
            return jrh.error(msgs.invalid_username);
        }
        if (!user.validPassword(password)) {
            return jrh.error(msgs.invalid_password);
        }
        if (user.isDeleted()) {
            return jrh.error(msgs.deleted);
        }
        const userToken = { id: user.id };
        const token = jwt.sign(
            userToken,
            keys.jwt.secretKey,
            { expiresIn: tokenExpiresIn }
        );
        return jrh.success({ token });
    } catch (e) {
        return handleCatch(e);
    }
};

const authSignin = async (username, email, password) => {
    const conditions = {
        $or: [{
            username,
            'email.address': email
        }]
    };
    try {
        const user = await User.findOne(conditions);
        if (user) {
            return jrh.error(msgs.user_already_exists);
        }
        const newUser = new User({
            username,
            password: '',
            email: { address: email }
        });
        if (!newUser) {
            return jrh.error(msgs.cant_create_new_user);
        }
        newUser.password = User.generateHash(password);
        await newUser.save();
        const userToken = { id: newUser.id };
        const token = jwt.sign(
            userToken, keys.jwt.secretKey,
            { expiresIn: tokenExpiresIn }
        );
        return jrh.success({ token });
    } catch (e) {
        return handleCatch(e);
    }
};

module.exports = { authLogin, authSignin };
