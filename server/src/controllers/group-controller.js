'use strict';

const { ObjectID } = require('mongodb');

const { User } = require('./../models/user-model');
const { Group } = require('./../models/group-model');
const { Product } = require('./../models/product-model');
const { Messenger } = require('./../models/messenger-model');

const jrh = require('./../utils/jsonResponseHandler');

const msgs = {
    catch: { msg: 'ops' },
    cant_create_new_group: { msg: 'cant_create_new_group' },
    invalid_product_id: { msg: 'invalid_product_id' },
    invalid_group_id: { msg: 'invalid_group_id' },
    invalid_msg_id: { msg: 'invalid_msg_id' },
    group_not_found: { msg: 'group_not_found' },
    group_deleted: { msg: 'group_deleted' },
    user_is_already_member: { msg: 'user_is_already_member' },
    user_is_blocked: { msg: 'user_is_blocked' },
    user_is_not_member: { msg: 'user_is_not_member' },
    product_has_same_block_status: { msg: 'product_has_same_block_status' },
    user_has_same_block_status: { msg: 'user_has_same_block_status' },
    product_cant_find_it: { msg: 'product_cant_find_it' },
    product_does_not_belongs_to_this_user: {
        msg: 'product_does_not_belongs_to_this_user'
    }
};

const checkID = (id) => ObjectID.isValid(id);

const isProductDeleted = (product) => product.isDeleted();
const isGroupdDeleted = (group) => group.isDeleted();

const excludeDeletedQuery = { 'statistics.deleted': { $exists: false } };

const handleCatch = (e) => {
    console.error(e);
    return jrh.fail(msgs.catch);
};

const checkMyGroupPermissions = (group, userId) => ((group.isPrivate()
    && group.isUserMember(userId) && !group.isUserBlocked(userId)) ||
    (!group.private && group.isUserMember(userId)
        && !group.isUserBlocked(userId)));

/**
 * It sends back all the groups we have
 */
const groupGetAll = async () => {
    try {
        const groups = await Group.find({}).select(excludeDeletedQuery);
        return jrh.success(groups, groups.length);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It creates a new group
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {Object} groupData - New group json data
 */
const groupCreate = async (user, groupData) => {
    try {
        const newGroup = new Group(groupData);
        if (!newGroup) {
            return jrh.error(msgs.cant_create_new_group);
        }
        newGroup.owner = user.id;
        newGroup.save();
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It deletes a valid group
 * @param {mongoose.Schema.Types.ObjectID} user - User Model
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 * TODO: Verificar se eu sou um admin e se não tem mais admins
 */
const groupDelete = async (user, groupId) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }

        const group = await Group.findById(groupId).select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }

        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }

        if (!group.isUserAdmin(user.id)) {
            return jrh.error(msgs.permission_denied);
        }

        user.deleteGroup(group);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It sends back only the groups that belongs to the logged user
 * @param {mongoose.Schema.Types.ObjectID} userId - User id
 */
const groupGetMine = async (userId) => {
    try {
        const groups = await Group.findById({ owner: userId })
            .select(excludeDeletedQuery);
        return jrh.success(groups, groups.length);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It sends back the groups this user is a member of
 * @param {mongoose.Model} user - User Model
 */
const groupGetAllParticipate = (user) => jrh
    .success(user.data.groups, user.data.groups.length);

/**
 * Find a group using filters
 * @param {Object} filtersData - JSON containing the filters
 */
const groupFilter = async (filterData) => {
    try {
        const { title } = filterData.body;
        const { description } = filterData.body;
        const { dietType } = filterData.type;

        const regexTitle = `/.*${title}.*/i`;
        const regexDesc = `/.*${description}.*/i`;

        const conditions = {
            title: regexTitle,
            'description.public': regexDesc,
            'settings.dietType': dietType
        };
        const groups = await Group.find(conditions).select(excludeDeletedQuery);
        return jrh.success(groups, groups.length);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It sends back a specific group
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 */
const groupGet = async (userId, groupId) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }

        const group = await Group.findById(groupId).select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }

        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }

        if (!checkMyGroupPermissions(group, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        return jrh.success(group);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It updates the attributes from a specific group
 * @param {mongoose.Schema.Types.ObjectID} userIdd - User Id
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 * @param {Object} updatedGroupData - A json containing the updated attributes
 */
const groupPatch = async (userId, groupId, updatedGroupData) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const group = await Group.findById(groupId).select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!group.isUserAdmin(userId)) {
            return jrh.error(msgs.permission_denied);
        }
        await group.updateGroup(updatedGroupData);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It sends back all the members from this specific group
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 */
const groupMembersGetAll = async (userId, groupId) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const group = await Group.findById(groupId)
            .select(excludeDeletedQuery)
            .populate('data.members');
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!checkMyGroupPermissions(group, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        return jrh.success(group.data.members, group.data.members.length);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It sends back all the blocked members from this specific group
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 */
const groupMembersBlockedGetAll = async (userId, groupId) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const group = await Group.findById(groupId)
            .select(excludeDeletedQuery)
            .populate('data.blocked.members');
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!group.isUserAdmin(userId)) {
            return jrh.error(msgs.permission_denied);
        }
        return jrh.success(
            group.data.blocked.members,
            group.data.blocked.members.length
        );
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It makes possible to the logged user to join an specific group
 * @param {mongoose.Schema.Types.ObjectID} user - User Model
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 * @param {String} accessToken - Access Token
 */
const groupMembersJoinPatch = async (user, groupId, accessToken) => {
    /* TODO: Before add any product to a group we should check if the product
     * is a valid product for that group. We do the by checking the dietType of
     * product and group
     */
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const group = await Group.findById(groupId).select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (group.isUserMember(user.id)) {
            return jrh.error(msgs.user_is_already_member);
        }
        if (group.isUserBlocked(user.id)) {
            return jrh.error(msgs.user_is_blocked);
        }
        if (!user.hasGroup(groupId)) {
            user.data.groups.push(groupId);
        }
        user.joinGroup(group);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It makes possible to the logged user leave an specific group
 * @param {mongoose.Schema.Types.ObjectID} user - User Model
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 */
const groupMembersLeavePatch = async (user, groupId) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        if (!user.hasGroup(groupId)) {
            user.data.groups.push(groupId);
        }
        const group = Group.findById(groupId).select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!group.isUserMember(user.id)) {
            return jrh.error(msgs.user_is_not_member);
        }
        user.leaveGroup(groupId);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It makes possible to an admin to block an specific member
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 * @param {Object} blockInfo - A json containing user id we have to block
 * and if we should block or unblock the specific user
 */
const groupMembersBlockPatch = async (userId, groupId, blockInfo) => {
    try {
        const { block } = blockInfo;
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const group = await Group.findById(groupId).select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!group.isUserAdmin(userId)) {
            return jrh.error(msgs.permission_denied);
        }
        if (block === 1 && !group.isUserBlocked(userId)) {
            group.blockUser(userId);
            return jrh.success();
        } else if (block === 0 && group.isUserBlocked(userId)) {
            group.unblockUser(userId);
            return jrh.success();
        }
        return jrh.error(msgs.error(msgs.user_has_same_block_status));
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It makes possible to an admin to kick an user from a specific group
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 * @param {Object} kickInfo - A json containing user id we have to kick
 */
const groupMembersKickPatch = async (userId, groupId, kickInfo) => {
    try {
        const { kickId } = kickInfo;
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        if (!checkID(kickId)) {
            return jrh.error(msgs.invalid_user_id);
        }
        const group = await Group.findById(groupId).select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!group.isUserAdmin(userId)) {
            return jrh.error(msgs.permission_denied);
        }
        if (group.isUserAdmin(kickId)) {
            return jrh.error(msgs.permission_denied);
        }
        const userToKick = await User.findById(kickId);
        if (!userToKick) {
            return jrh.error(msgs.user_is_not_member);
        }
        userToKick.leaveGroup(groupId);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It sends back every product from this group
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 */
const groupProductsGetAll = async (userId, groupId) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const group = await Group.findById(groupId)
            .select(excludeDeletedQuery)
            .populate('products');
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!checkMyGroupPermissions(group, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        return jrh.success(group.data.products, group.data.products.length);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It sends back every blocked product from this group
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 */
const groupProductsBlockedGetAll = async (userId, groupId) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const group = await Group.findById(groupId)
            .select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!group.isUserAdmin(userId)) {
            return jrh.error(msgs.permission_denied);
        }
        return jrh.success(
            group.data.blocked.products,
            group.data.blocked.products.length
        );
    } catch (e) {
        return handleCatch(e);
    }
};


/**
 * It sends back every product from this project that belongs to the logged user
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {mongoose.Schema.Types.ObjectID} groupIp - Group Id
 */
const groupProductsGetMine = async (userId, groupId) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const group = Group.findById(groupId);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!checkMyGroupPermissions(group, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        const myProducts = group.data.products
            .filter(product => product.owner === userId);
        return jrh.success(myProducts, myProducts.length);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It adds a valid user product to a specific group
 * @param {mongoose.Schema.Types.ObjectID} user - User Model
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 * @param {Object} productData - A json containing the product Id
 */
const groupProductsAddPatch = async (user, groupId, productData) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const { productId } = productData;
        if (!checkID(productId)) {
            return msgs.invalid_product_id;
        }
        const group = await Group.findById(groupId).select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!checkMyGroupPermissions(group, user.id)) {
            return jrh.error(msgs.permission_denied);
        }
        const product = await Product.findById(productId)
            .select(excludeDeletedQuery);
        if (!product) {
            return jrh.error(msgs.product_cant_find_it);
        }
        if (!user.isProductMine(productId)) {
            jrh.error(msgs.product_does_not_belongs_to_this_user);
        }
        if (isProductDeleted(product)) {
            return jrh.error(msgs.deleted);
        }
        product.joinGroup(group);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It removes a valid product from a specific group
 * @param {mongoose.Schema.Types.ObjectID} user - User Model
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 * @param {Object} productData - A json containing the product Id
 */
const groupProductsRemove = async (user, groupId, productData) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const { productId } = productData;
        if (!checkID(productId)) {
            return jrh.error(msgs.invalid_product_id);
        }
        const group = Group.findById(groupId).select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!checkMyGroupPermissions(group, user.id)) {
            return jrh.error(msgs.permission_denied);
        }
        const product = Product.findOne({
            _id: productId,
            groups: productId
        });
        if (!product) {
            return jrh.error(msgs.product_cant_find_it);
        }
        if (!user.isProductMine(productId) && !user.isUserAdmin()) {
            return jrh.error(msgs.product_does_not_belongs_to_this_user);
        }
        product.leaveGroup(group);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It makes possible to an admin to block an specific product
 * @param {mongoose.Schema.Types.ObjectID} productId - Product Id
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group Id
 * @param {Object} blockInfo - A json containing user id we have to block
 * and if we should block or unblock the specific product
 */

const groupProductsBlockPatch = async (productId, groupId, blockInfo) => {
    if (!checkID(productId)) {
        return jrh.error(msgs.invalid_product_id);
    }
    if (!checkID(groupId)) {
        return jrh.error(msgs.invalid_group_id);
    }
    const group = await Group.findById(groupId);
    if (!group) {
        return jrh.error(msgs.group_not_found);
    }
    const { block } = blockInfo;
    if (block === 1 && !group.isProductBlocked(productId)) {
        group.blockProduct(productId);
        return jrh.success();
    } else if (block === 0 && group.isProductBlocked(productId)) {
        group.unblockProproduct(productId);
        return jrh.success();
    }
    return jrh.error(msgs.error(msgs.product_has_same_block_status));
};

/**
 * Get all messages sent to this group
 * @params {mongoose.Schema.Types.ObjectId} groupId - Group Id
 * @params {mongoose.Schema.Types.ObjectId} userId - User Id
 */
const groupMessagesGetAll = async (groupId, userId) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const group = await Group.findById(groupId);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!checkMyGroupPermissions(group, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        const messages = await Messenger.find({
            'header.to': groupId,
            'flags.receiverFlags.acquired': false
        });
        return jrh.success(messages, messages.length);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * Send a message to this group
 * @param {mongoose.Schema.Types.ObjectID} groupId - Group ID
 * @param {mongoose.Schame.Types.ObjectID} userId - User ID
 * @param {String} content - Message to Send
 */
const groupMessagesSendPatch = async (groupId, userId, content) => {
    try {
        if (!checkID(groupId)) {
            return jrh.error(msgs.invalid_group_id);
        }
        const group = await Group.findById(groupId).select(excludeDeletedQuery);
        if (!group) {
            return jrh.error(msgs.group_not_found);
        }
        if (isGroupdDeleted(group)) {
            return jrh.error(msgs.deleted);
        }
        if (!checkMyGroupPermissions(group, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        const newMessageToSend = new Messenger();
        newMessageToSend.setAsMessage();
        newMessageToSend.setAsGroupMessage();
        newMessageToSend.setFrom(userId);
        newMessageToSend.setTo(groupId);
        newMessageToSend.setContent(content);
        await newMessageToSend.save();
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

module.exports = {
    groupGetAll,
    groupCreate,
    groupDelete,
    groupGetMine,
    groupGetAllParticipate,
    groupFilter,
    groupGet,
    groupPatch,
    groupMembersGetAll,
    groupMembersBlockedGetAll,
    groupMembersJoinPatch,
    groupMembersLeavePatch,
    groupMembersBlockPatch,
    groupMembersKickPatch,
    groupProductsGetAll,
    groupProductsBlockedGetAll,
    groupProductsGetMine,
    groupProductsAddPatch,
    groupProductsBlockPatch,
    groupProductsRemove,
    groupMessagesGetAll,
    groupMessagesSendPatch
};
