'use strict';

const { ObjectID } = require('mongodb');

const { Product } = require('./../models/product-model');

const jrh = require('./../utils/jsonResponseHandler');

const msgs = {
    catch: { msg: 'ops' },
    cant_create_new_product: { msg: 'cant_create_new_product' },
    invalid_product_id: { msg: 'invalid_product_id' },
    invalid_args: { msg: 'invalid_args' },
    cant_find_product: { msg: 'cant_find_product' },
    permission_denied: { msg: 'permission_denied' },
    deleted: { msg: 'product_deleted' }
};

const checkID = (id) => ObjectID.isValid(id);

const isProductDeleted = (product) => product.isDeleted();
const amIACollector = (product, userId) => product.amIACollector(userId);
const isProductMine = (product, userId) => product.isThisProductMine(userId);

const handleCatch = (res, e) => {
    console.error(e);
    return jrh.fail(msgs.catch);
};

const excludeDeletedQuery = { 'statistics.deleted': { $exists: false } };

/**
 * It sends back all the products we have
 */
const productGetAll = async () => {
    try {
        const products = await Product.find({}).select(excludeDeletedQuery);
        return jrh.success(products, products.len);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It creates a new product
 * @param {mongoose.Schema} user - User Model
 * @param {Object} productData - New product JSON
 */
const productCreate = async (user, productData) => {
    try {
        const newProduct = new Product(productData);
        if (!newProduct) {
            return jrh.error(msgs.cant_create_new_product);
        }
        newProduct.owner = user.id;
        const savedProduct = await newProduct.save();
        if (!savedProduct) {
            return jrh.error(msgs.cant_create_new_product);
        }
        return jrh.success(savedProduct);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It sends back a specific user products
 * @param {mongoose.Scham.Types.ObjectID} userId - User Id
 */
const productGetMine = async (userId) => {
    try {
        const products = Product.find({ owner: userId })
            .select(excludeDeletedQuery);
        return jrh.success(products, products.length);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It sends back the product info
 * @param {mongoose.Schema.Types.ObjectID} productId - Product Id
 */
const productGet = async (productId) => {
    try {
        if (!checkID(productId)) {
            return jrh.error(msgs.invalid_product_id);
        }
        const product = await Product.findById(productId)
            .select(excludeDeletedQuery);
        if (!product) {
            return jrh.error(msgs.cant_find_product);
        }
        if (isProductDeleted(product)) {
            return jrh.error(msgs.deleted);
        }
        return jrh.success(product);
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * Update a user product info
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {mongoose.Schema.Types.ObjectID} productId - Product Id
 * @param {Object} - JSON - Updated product info
 */
const productPatch = async (userId, productId, updatedData) => {
    try {
        if (!checkID(productId)) {
            return jrh.error(msgs.invalid_product_id);
        }
        const product = await Product.findById(productId)
            .select(excludeDeletedQuery);
        if (!product) {
            return jrh.error(msgs.cant_find_product);
        }
        if (isProductDeleted(product)) {
            return jrh.error(msgs.deleted);
        }
        if (!isProductMine(product, userId)) {
            return jrh.error(msgs.permission_denied);
        }

        await product.updateProduct(updatedData);

        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * It will delete a product
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 * @param {mongoose.Schema.Types.ObjectID} productId - Product Id
 */
const productDelete = async (userId, productId) => {
    try {
        if (!checkID(productId)) {
            return jrh.error(msgs.invalid_product_id);
        }
        const product = Product.findById(productId)
            .select(excludeDeletedQuery);
        if (!product) {
            return jrh.error(msgs.cant_find_product);
        }
        if (isProductDeleted(product)) {
            return jrh.error(msgs.deleted);
        }
        if (!isProductMine(product, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        product.deleteProduct(userId);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * Signup for a product pickup
 */
const productPickUpSignUp = async (userId, productId, message) => {
    try {
        if (!checkID(productId)) {
            return jrh.error(msgs.invalid_product_id);
        }
        const product = Product.findById(productId);
        if (!product) {
            return jrh.error(msgs.cant_find_product);
        }
        if (!isProductMine(product, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        if (!amIACollector(product, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        await product.signForPickUp(userId, message);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

/**
 * Cancel a product collection
 */
const productPickUpCancel = async (userId, productId, reason) => {
    try {
        if (!checkID(productId)) {
            return jrh.error(msgs.invalid_product_id);
        }
        const product = Product.findById(productId);
        if (!product) {
            return jrh.error(msgs.cant_find_product);
        }
        if (!isProductMine(product, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        if (!amIACollector(product, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        await product.cancelPickUp(userId, reason);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

const productGetAllCollectors = async (userId, productId) => {
    try {
        if (checkID(productId)) {
            return jrh.error(msgs.invalid_product_id);
        }
        const product = Product.findById(productId)
            .select(excludeDeletedQuery);
        if (!product) {
            return jrh.error(msgs.cant_find_product);
        }
        if (!isProductMine(product, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        return jrh.success(
            product.data.collectors,
            product.collectors.groups.length
        );
    } catch (e) {
        return handleCatch(e);
    }
};

const productGetAllGroups = async (userId, productId) => {
    try {
        if (!checkID(productId)) {
            return jrh.error(msgs.invalid_product_id);
        }
        const product = await Product.findById(productId)
            .select(excludeDeletedQuery);
        if (!product) {
            return jrh.error(msgs.cant_find_product);
        }
        if (!isProductMine(product, userId)) {
            return jrh.error(msgs.permission_denied);
        }
        return jrh.success(product.data.groups, product.data.groups.length);
    } catch (e) {
        return handleCatch(e);
    }
};

const productSendBroadcastMessage = async (product, message) => {
    if (!product) {
        return jrh.error(msgs.cant_find_product);
    }
    if (!message) {
        return jrh.error(msgs.invalid_args);
    }
    await product.sendBroadcastMessage(message);
    return jrh.success();
};

module.exports = {
    productGet,
    productGetAll,
    productCreate,
    productGetMine,
    productPatch,
    productDelete,
    productPickUpSignUp,
    productPickUpCancel,
    productGetAllCollectors,
    productGetAllGroups,
    productSendBroadcastMessage
};
