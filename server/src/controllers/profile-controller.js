'use strict';

const { ObjectID } = require('mongodb');

const { User } = require('./../models/user-model');
const { Messenger } = require('./../models/messenger-model');

const jrh = require('./../utils/jsonResponseHandler');

const msgs = {
    catch: { msg: 'ops' },
    cant_update_user_profile: { msg: 'cant_update_user_profile' },
    cant_update_settings: { msg: 'cant_update_settings' },
    invalid_user_id: { msg: 'invalid_user_id' },
    unknown_user: { msg: 'unknown_user' },
    deleted_user: { msg: 'deleted_user' }
};

const checkId = (id) => ObjectID.isValid(id);

const isUserDeleted = (user) => user.isDeleted();
const isMessageMine = (messenger) => messenger.isMessageMine();

const handleCatch = (e) => {
    console.error(e);
    return jrh.fail(msgs.catch);
};

const excludeDeletedQuery = { 'statistics.deleted': { $exists: false } };

const profileGetUser = async (userId) => {
    try {
        if (!checkId(userId)) {
            return jrh.error(msgs.invalid_user_id);
        }
        const user = await User.findById(userId).select(excludeDeletedQuery);
        if (!user) {
            return jrh.error(msgs.unknown_user);
        }
        if (isUserDeleted) {
            return jrh.error(msgs.deleted_user);
        }
        const data = {
            username: user.username,
            statiscts: user.profile.statiscts,
            profile: user.profile || {}
        };
        return jrh.success(data);
    } catch (e) {
        return handleCatch(e);
    }
};

const profileGet = (user) => {
    const data = {
        username: user.username,
        statiscts: user.profile.statiscts,
        profile: user.profile || {}
    };
    return jrh.success(data);
};

const profilePatch = async (user, updatedData) => {
    const updatedProfile = await user.updateProfile(updatedData);
    try {
        if (!updatedProfile) {
            return jrh.error(msgs.cant_update_user_profile);
        }
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

const profileDelete = async (user) => {
    try {
        await user.deleteProfile();
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

const profileSettingsGet = (user) => {
    const data = {
        settings: user.settings
    };
    return jrh.success(data);
};

const profileSettingsPatch = async (user, updatedData) => {
    try {
        const settings = await user.updateSettings();
        if (!settings) {
            return jrh.error(msgs.cant_update_settings);
        }
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

const profileMessageGetSentAll = async (userId) => {
    try {
        const data = Messenger.find({
            owner: userId,
            'flags.senderFlags.deleted': { $exists: false }
        });
        return jrh.success(data, data.length);
    } catch (e) {
        return handleCatch(e);
    }
};

const profileMessageGetRecvfAll = async (userId) => {
    try {
        const data = await Messenger.find({
            'header.to': userId,
            'flags.receiverFlags.deleted': { $exists: false }
        });
        return jrh.success(data, data.length);
    } catch (e) {
        return handleCatch(e);
    }
};

const profileMessageSendPatch = async (userId, toId, content, urgent) => {
    try {
        const newMessageToSend = new Messenger();
        newMessageToSend.setAsMessage();
        newMessageToSend.setAsUserMessage();
        newMessageToSend.setFrom(userId);
        newMessageToSend.setTo(toId);
        newMessageToSend.setContent(content);
        newMessageToSend.setUrgent(urgent);
        await newMessageToSend.save();
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

const profileMessageMarkAsReadPatch = async (userId, messagesIds) => {
    try {
        const messagesToMarkAsRead = messagesIds
            .filter(message => checkId(message.id) && isMessageMine(userId));
        const messagesFromDb = Messenger.findById(messagesToMarkAsRead);
        const messagesToUpdatePromises = messagesFromDb
            .map(message => message.markAsRead());
        await Promise.all(messagesToUpdatePromises);
        return jrh.success();
    } catch (e) {
        return handleCatch(e);
    }
};

module.exports = {
    profileGetUser,
    profileGet,
    profilePatch,
    profileDelete,
    profileSettingsGet,
    profileSettingsPatch,
    profileMessageGetSentAll,
    profileMessageGetRecvfAll,
    profileMessageSendPatch,
    profileMessageMarkAsReadPatch
};
