'use strict';

module.exports = {
    fail(data) {
        return {
            type: 'fail',
            code: 500,
            json: data
        };
    },
    error(data) {
        return {
            type: 'error',
            conse: 400,
            json: data
        };
    },
    success(data, len) {
        return {
            type: 'success',
            code: 200,
            json: {
                len,
                data
            }
        };
    }
};
