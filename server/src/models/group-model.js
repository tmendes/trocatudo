'use strict';

const mongoose = require('mongoose');

const { Schema } = mongoose;

const groupSchema = new Schema({
    owner: { type: Schema.Types.ObjectId, ref: 'User' },
    info: {
        name: {
            type: String,
            required: true,
            trim: true,
            minlength: 5,
            lowercase: true,
            unique: true
        },
        photoUrl: String,
        language: String,
        title: {
            type: String,
            required: true,
            minlength: 5
        },
        description: {
            public: String,
            private: String
        },
        location: {
            latitude: String,
            longitude: String
        }
    },
    settings: {
        private: {
            type: Boolean,
            default: false
        },
        status: {
            type: Number,
            default: 0
        },
        dietType: {
            type: Schema.Types.ObjectId,
            ref: 'Diet'
        }
    },
    tokens: {
        invitations: [String]
    },
    data: {
        products: [{ type: Schema.Types.ObjectId, ref: 'Product' }],
        members: [{ type: Schema.Types.ObjectId, ref: 'User' }],
        blocked: {
            members: [{ type: Schema.Types.ObjectId, ref: 'User' }],
            products: [{ type: Schema.Types.ObjectId, ref: 'Product' }]
        }
    },
    statistics: {
        trustLevel: Number,
        statisfactionLevel: Number,
        createdAt: { type: Date, default: Date.now() },
        lastEditAt: Date,
        deleted: Date
    }
});

/**
 * Check if the group is deleted
 * @returns {Date} - if it is deleted
 */
groupSchema.methods.isDeleted = function () {
    return this.statistics.deleted;
};

/**
 * Check if the group is private
 */
groupSchema.methods.isPrivate = function () {
    return this.private;
};

/**
 * Update group data
 * @params {Object} updatedData - Group new data
 */
groupSchema.methods.updateGroup = function (updatedData) {
    return this.update({
        $set: {
            info: updatedData.info,
            settings: updatedData.settings,
            'statistics.lastEditAt': Date.now()
        }
    }, { new: true });
};

/**
 * Check if an user is a member of a specific group
 * @param {mongoose.Schema} group - Group Mongoose Model
 * @param {mongoose.Schema.Types.ObjectID} userId - User ID
 * @returns {Boolean} - User belongs or not to this group
 */
groupSchema.methods.isUserMember = function (userId) {
    return this.data.members.some(member => member.equals(userId));
};

/**
 * Check if an user is a blocked member of a specific group
 * @param {mongoose.Schema.Types.ObjectID} userId - User ID
 * @returns {Boolean} - User belongs or not to this group
 */
groupSchema.methods.isUserBlocked = function (userId) {
    return this.data.blocked.members.some(member => member.equals(userId));
};

/**
 * Check if a product is blocked
 * @param {mongoose.Schema.Types.ObjectID} productId - Product ID
 * @returns {Boolean} - User belongs or not to this group
 */
groupSchema.methods.isProductBlocked = function (productId) {
    return this.data.blocked.products.some(member => member.equals(productId));
};

/**
 * Check if an user is the owner
 * @param {mongoose.Schema} group - Group Mongoose Model
 * @param {mongoose.Schema.Types.ObjectID} userId - User ID
 * @returns {Boolean} - User belongs or not to this group
 */
groupSchema.methods.isUserAdmin = function (userId) {
    return this.owner === userId;
};

/**
 * Block a user
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 */
groupSchema.methods.blockUser = function (userId) {
    this.update({
        $pullAll: { 'data.members': [userId] },
        $pull: { 'data.blocked.members': [userId] }
    });
};

/**
 * Unblock a user
 * @param {mongoose.Schema.Types.ObjectID} userId - User Id
 */
groupSchema.methods.unblockUser = function (userId) {
    this.update({
        $pullAll: { 'data.blocked.members': [userId] },
        $pull: { 'data.members': [userId] }
    });
};

/**
 * Block a product
 * @param {mongoose.Schema.Types.ObjectID} productId - Product Id
 */
groupSchema.methods.blockProduct = function (productId) {
    this.update({
        $pullAll: { 'data.products': [productId] },
        $pull: { 'data.blocked.products': [productId] }
    });
};

/**
 * Unblock a product
 * @param {mongoose.Schema.Types.ObjectID} productId - Product Id
 */
groupSchema.methods.unblockProduct = function (productId) {
    this.update({
        $pullAll: { 'data.blocked.products': [productId] },
        $pull: { 'data.products': [productId] }
    });
};

/**
 * Mark group as deleted. The data should be removed after a specifed time
 */
groupSchema.methods.deleteGroup = async function () {
    this.update({
        $set: { 'statistics.deleted': Date.now() }
    });
};

/* Compiles a Schema into a model */
const Group = mongoose.model('Group', groupSchema);

module.exports = { Group };
