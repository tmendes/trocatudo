'use strict';

const mongoose = require('mongoose');

const { Schema } = mongoose;

const commonFlags = {
    urgent: { type: Boolean, default: false },
    archived: { type: Boolean, default: false },
    delete: Date
};

const messengerSchema = new Schema({
    owner: Schema.Types.ObjectId,
    header: {
        from: { type: Schema.Types.ObjectId, ref: 'User' },
        to: { type: Schema.Types.ObjectId, ref: 'User' }
    },
    data: {
        contentType: {
            enum: [
                'Request',
                'CancelRequest',
                'Message'
            ],
            default: 'Message',
            description: 'can only be one of the enum values and is required'
        },
        content: { type: String },
        readAt: Date
    },
    flags: {
        broadcast: { type: Boolean, default: false },
        senderFlags: {
            commonFlags
        },
        receiverFlags: {
            commonFlags,
            acquired: { type: Boolean, default: false }
        },
        receiverType: {
            enum: [
                'User',
                'Group',
                'Product'
            ],
            description: 'can only be one of the enum values and is required'
        }
    },
    statistics: {
        createdAt: { type: Date, default: Date.now() }
    }
});

messengerSchema.methods.isMessageMine = function (userId) {
    return this.header.from.id === userId;
};

messengerSchema.methods.setFrom = function (userId) {
    this.update({ $set: { 'header.from': userId } });
};

messengerSchema.methods.setTo = function (userId) {
    this.update({ $set: { 'header.to': userId } });
};

messengerSchema.methods.setAsRequest = function () {
    this.update({ $set: { 'data.contentType': 'Request' } });
};

messengerSchema.methods.setAsRequestCancelation = function () {
    this.update({ $set: { 'data.contentType': 'CancelRequest' } });
};

messengerSchema.methods.setAsMessage = function () {
    this.update({ $set: { 'data.contentType': 'Message' } });
};

messengerSchema.methods.setAsBroadcast = function () {
    this.update({ $set: { 'data.flags.broadcast': true } });
};

messengerSchema.methods.setContentMessage = function (message) {
    this.update({ $set: { 'data.content': message } });
};

messengerSchema.methods.setAsUserMessage = function (message) {
    this.update({ set: { 'data.flags.receiverType': 'User' } });
};

messengerSchema.methods.setAsGroupMessage = function (message) {
    this.update({ set: { 'data.flags.receiverType': 'Group' } });
};

messengerSchema.methods.setAsProductMessage = function (message) {
    this.update({ set: { 'data.flags.receiverType': 'Product' } });
};

messengerSchema.methods.setAsRead = function (userId) {
    this.update({ $set: { 'data.readAt': Date.now() } });
};

messengerSchema.methods.setAsUrgent = function (userId) {
    if (this.header.from.id === userId) {
        this.update({ $set: { 'data.flags.senderFlags.urgent': true } });
    } else if (this.header.to.id === userId) {
        this.update({ $set: { 'data.flags.receiverFlags.urgent': true } });
    }
};

messengerSchema.methods.setAsArchived = function (userId) {
    if (this.header.from.id === userId) {
        this.update({ $set: { 'data.flags.senderFlags.archived': true } });
    } else if (this.header.to.id === userId) {
        this.update({ $set: { 'data.flags.receiverFlags.archived': true } });
    }
};

messengerSchema.methods.delete = function (userId) {
    if (this.header.from.id === userId) {
        this.update({ $set: { 'data.flags.senderFlags.deleted': true } });
    } else if (this.header.to.id === userId) {
        this.update({ $set: { 'data.flags.receiverFlags.deleted': true } });
    }
};

messengerSchema.methods.forceDelete = function () {
    this.update({ $set: { 'data.flags.senderFlags.deleted': true } });
};

messengerSchema.methods.setAsAcquired = function (userId) {
    if (this.header.to.id === userId) {
        this.update({ $set: { 'data.flags.receiverFlags.acquired': true } });
    }
};

/* Compiles a Schema into a Model */
const Messenger = mongoose.model('Messenger', messengerSchema);

module.exports = { Messenger };
