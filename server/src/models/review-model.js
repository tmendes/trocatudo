'use strict';

const mongoose = require('mongoose');

const { Schema } = mongoose;

const reviewSchema = new Schema({
    header: {
        from: { type: Schema.Types.ObjectId, ref: 'User' },
        to: { type: Schema.Types.ObjectId, ref: 'User' }
    },
    data: {
        message: String,
        rating: Number,
        ranking: Number
    },
    statistics: {
        createdAt: { type: Date, default: Date.now() },
        lastEdit: Date
    }
});

const Review = mongoose.model('Review', reviewSchema);

module.exports = { Review };
