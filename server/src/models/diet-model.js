'use strict';

const mongoose = require('mongoose');

const { Schema } = mongoose;

const dietSchema = new Schema({
    dietType: {
        enum: [
            'Vegan',
            'Vegetarian',
            'Lacto-Vegetarian',
            'Fruitarians',
            'Macrobiotic',
            'Raw',
            'Pescatarians',
            'Others'
        ],
        default: 'Others',
        description: 'can only be one of the enum values and is required'
    },
    allergiyType: [{
        enum: [
            'Milk',
            'Eggs',
            'Peanuts',
            'Tree nuts',
            'Seafood',
            'Shellfish',
            'Soy',
            'Wheat',
            'Seeds',
            'Pecans',
            'Pistachios',
            'Pine nuts',
            'Coconuts',
            'Walnuts',
            'Balsam of Peru',
            'None'
        ],
        default: 'None',
        description: 'can only be one of the enum values and is required'
    }],
    intoleranceTypes: [{
        enum: [
            'Lactose',
            'Celiac',
            'None'
        ],
        default: 'None',
        description: 'can only be one of the enum values and is required'
    }]
});

/* Compiles a Schema into a Model */
const Diet = mongoose.model('Diet', dietSchema);

module.exports = { Diet };
