'use strict';

const mongoose = require('mongoose');

const { Schema } = mongoose;

const partnerShipSchema = new Schema({

});

/* Compiles a Schema into a Model */
const PartnerShip = mongoose.model('PartnerShip', partnerShipSchema);

module.exports = { PartnerShip };
