'use strict';

const mongoose = require('mongoose');

const { Schema } = mongoose;

const { Messenger } = require('./../models/messenger-model');

const productSchema = new Schema({
    owner: { type: Schema.Types.ObjectId, ref: 'User' },
    info: {
        name: {
            type: String,
            required: true,
            minlength: 5
        },
        photo: String,
        description: { type: String, required: true },
        weigth: Number,
        dietType: {
            type: Schema.Types.ObjectId,
            ref: 'Diet'
        },
        keepAnEyeIngredients: [{
            type: Schema.Types.ObjectId,
            ref: 'DangerousFood'
        }]
    },
    data: {
        groups: [{ type: Schema.Types.ObjectId, ref: 'Group' }],
        collectors: [{ type: Schema.Types.ObjectId, req: 'User' }]
    },
    pickUpInfo: {
        quantity: { type: Number, required: true },
        hide: Boolean,
        aviability: {
            mon: {
                morning: { type: Boolean, default: false },
                afternoon: { type: Boolean, default: false }
            },
            tur: {
                morning: { type: Boolean, default: false },
                afternoon: { type: Boolean, default: false }
            },
            wed: {
                morning: { type: Boolean, default: false },
                afternoon: { type: Boolean, default: false }
            },
            thu: {
                morning: { type: Boolean, default: false },
                afternoon: { type: Boolean, default: false }
            },
            fri: {
                morning: { type: Boolean, default: false },
                afternoon: { type: Boolean, default: false }
            },
            sat: {
                morning: { type: Boolean, default: false },
                afternoon: { type: Boolean, default: false }
            },
            sun: {
                morning: { type: Boolean, default: false },
                afternoon: { type: Boolean, default: false }
            }
        },
        expiresAt: { type: Date, required: true }
    },
    statistics: {
        createdAt: { type: Date, default: Date.now() },
        lastEditAt: Date,
        deleted: Date
    }
});

productSchema.methods.amIACollector = function (userId) {
    return this.data.collectors.some(user => user.equal(userId));
};

productSchema.methods.sendBroadcastMessage = async function (content) {
    this.data.collectors.forEach(async function (userId) {
        await new Messenger()
            .setFrom(this.owner)
            .setTo(userId)
            .setAsMessage()
            .setContentMessage(content)
            .save();
    });
};

productSchema.methods.isDeleted = function () {
    return this.statistics.deleted;
};

productSchema.methods.isProductMine = function (userId) {
    return this.owner === userId;
};

productSchema.methods.updateProduct = function (updatedData) {
    return this.update(
        {
            $set: {
                updatedData,
                'statiscts.lastEditAt': Date.now()
            }
        },
        { new: true }
    );
};

productSchema.methods.joinGroup = async function (group) {
    await group.update({
        $push: { 'data.products': [this.id] }
    });
    await this.update({
        $push: { 'data.groups': [group.id] }
    });
};

productSchema.methods.leaveGroup = async function (group) {
    await group.update({
        $pullAll: { 'data.products': [this.id] }
    });
    await this.update({
        $pullAll: { 'data.groups': [group.id] }
    });
};

/**
 * Mark a product as deleted. The data should be removed after a specifed time
 */
productSchema.methods.deleteProduct = async function (user) {
    await this.update({
        $set: { 'statistics.deleted': Date.now() }
    });
    await user.update({
        $pushAll: { 'data.products': [this.id] }
    });
};

/**
 * When the user sign up for a pick up.
 * The user who owns the product should receive a message about this new pickup
 * @param {mongoose.model} user - User Model
 * @param { String } pickupMessage - PickUp Message
 */
productSchema.methods.signForPickUp = async function (user, pickupMessage) {
    await user.update({
        $push: { 'data.pickups': [this.id] }
    });
    await this.update({
        $push: { 'data.collectors': [user.id] }
    });
    const message = new Messenger();
    message.setFrom(user.id);
    message.setTo(this.owner);
    message.setAsRequest();
    message.setContentMessage(pickupMessage);
    await message.save();
};

/**
 * When the user cancel a pick up
 * The user who owns the product should receive a message about it
 * @param {mongoose.Schema.Types.ObjectId} user - Product Model
 * @param {String} reason - A message
 */
productSchema.methods.cancelPickUp = async function (user, reason) {
    await user.update({
        $pullAll: { 'data.pickups': [this.id] }
    });
    await this.update({
        $pullAll: { 'data.collectors': [user.id] }
    });
    const message = new Messenger();
    message.setFrom(user.id);
    message.setTo(this.owner);
    message.setAsRequestCancelation();
    message.setContentMessage(reason);
    await message.save();
};

/* Compiles a Schema into a Model */
const Product = mongoose.model('Product', productSchema);

module.exports = { Product };
