'use strict';

const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');

const { Schema } = mongoose;

const { Messenger } = require('./../models/messenger-model');

const notifications = {
    offerings: { type: Boolean, default: false },
    messages: { type: Boolean, default: false },
    notifications: { type: Boolean, default: false },
    reviews: { type: Boolean, default: false },
    newPickUp: { type: Boolean, default: false },
    reminders: {
        pickup: { type: Boolean, default: false },
        collectors: { type: Boolean, default: false }
    }
};

const userSchema = new Schema({
    username: {
        type: String,
        trim: true,
        minlength: 5,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        minlength: 8,
        required: true
    },
    email: {
        confimed: { type: Boolean, default: false },
        address: {
            type: String,
            trim: true,
            required: true,
            lowercase: true,
            unique: 'Email exists already'
        }
    },
    profile: {
        dietType: {
            type: Schema.Types.ObjectId,
            ref: 'Diet'
        },
        anyAllergies: Boolean,
        name: {
            first: String,
            last: String
        },
        document: { doctype: String, docvalue: String },
        photo: String,
        about: String,
        gender: Number,
        birthday: Date,
        location: {
            city: String,
            state: String,
            country: String
        },
        phone: String,
        socialNetwork: {
            instagram: String,
            facebook: String,
            google: String,
            snapchat: String
        }
    },
    data: {
        groups: [{ type: Schema.Types.ObjectId, ref: 'Group' }],
        pickups: [{ type: Schema.Types.ObjectId, ref: 'Product' }]
    },
    tokens: {
        emailConfimation: String,
        resetPassword: String,
        removeProfile: String
    },
    settings: {
        profile: {
            public: { type: Boolean, default: false }
        },
        notify: {
            email: {
                notifications
            },
            app: {
                notifications
            }
        }
    },
    role: {
        type: [{
            type: String,
            enum: ['user', 'business', 'ong', 'admin', 'supended'],
            default: ['user']
        }]
    },
    statistics: {
        trustLevel: Number,
        satisfectionLevel: Number,
        created: { type: Date, default: Date.now() },
        lastEditAt: Date,
        deleted: Date,
        seen: Date
    }
});

/**
 * Generates a hash string to save user's password
 * @param {String} password - User's password
 * @return {String} User's hash password
 */
userSchema.statics.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
};

/**
 * Validates a passord hash string
 * @param {String} hash - User's hash password
 * @return {Boolean} If this hash is the user's password or not
 */
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

/**
 * Update profile
 * @param {Object} updatedData - Profile new Data
 */
userSchema.methods.updateProfile = function (updatedData) {
    return this.update({
        $set: {
            profile: updatedData,
            'statiscts.lastEditAt': Date.now()
        }
    }, { new: true });
};

/**
 * Update profile settings
 * @param {Object} updatedData - Settings new Data
 */
userSchema.methods.updateSettings = function (updatedData) {
    return this.update({
        $set: {
            settings: updatedData,
            'statiscts.lastEditAt': Date.now()
        }
    }, { new: true });
};

/**
 * Check if the user is deleted or not
 */
userSchema.methods.isDeleted = function () {
    return this.statistics.deleted;
};

/**
 * Check if the user has a group
 * @param {mongoose.Schema.Types.ObjectId} groupIp - Group Id
 * @returns {Boolean} - User has or not to this groupr
 */
userSchema.methods.hasGroup = function (groupId) {
    return this.data.groups.some(group => group.equals(groupId));
};

/**
 * Check if the user has a pickup for a specific product
 * @param {mongoose.Schema.Types.ObjectId} productId - Product Id
 * @returns {Boolean} - User has or has not this pickup
 */
userSchema.methods.hasPickup = function (productId) {
    return this.data.pickups.some(product => product.equals(productId));
};

/**
 * When the user joins a group
 * @param {mongoose.Schema.Types.ObjectId} group - Group Model
 */
userSchema.methods.joinGroup = async function (group) {
    await this.update({
        push: { 'data.groups': [group.id] }
    });
    await group.update({
        push: { 'data.members': [this.id] }
    });
};

/**
 * When the user leave a group
 * Whenever a user decide to leave a group all of her/his products that are
 * registered at that specific group will be removed from the group
 * If the user is blocked she/he will stay blocked even if they leave the group
 * The same is valid for her/his blocked products
 * @param {mongoose.Schema.Types.ObjectId} group - Group Model
 */
userSchema.methods.leaveGroup = async function (group) {
    await this.update({
        $pullAll: { 'data.groups': group.id }
    });
    await group.update({
        $pullAll: {
            'data.members': [this.id]
        }
    });
};

/**
 * Whenever the user has something to say to another user
 * @param {mongoose.Schema.Types.ObjectId} iserToId - userTo ID
 * @param {content} content - Message to send
 * @param {Boolean} urgent - Is this message urgent
 */
userSchema.methods.sendUserAMessage = async function (
    userToId,
    content,
    urgent
) {
    const message = new Messenger();
    message.setFrom(this.id);
    message.setTo(userToId);
    message.setAsMessage();
    message.setAsUrgent(urgent);
    message.setContentMessage(content);
    await message.save();
    await this.update({
        $pushAll: { 'data.messages': [message.id] }
    });
};

/**
 * Mark user as deleted. The data should be removed after a specifed time
 */
userSchema.methods.deleteUser = async function () {
    this.update({
        $set: { 'statistics.delete': Date.now() }
    });
};

/* Compiles a Schema into a model */
const User = mongoose.model('User', userSchema);

module.exports = { User };
