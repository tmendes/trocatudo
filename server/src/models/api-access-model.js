'use strict';

const mongoose = require('mongoose');

const { Schema } = mongoose;

const apiAccessSchema = new Schema({
    tokens: {
        access: [String]
    }
});

/* Compiles a Schema into a Model */
const ApiAccess = mongoose.model('ApiAccess', apiAccessSchema);

module.exports = { ApiAccess };
