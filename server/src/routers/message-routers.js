'use strict';

const router = require('express').Router();
const passport = require('passport');

const authOptions = { session: false };

router.get(
    '/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {

    }
).get(
    '/$id',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {

    }
).post(
    '/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {

    }
).patch(
    '/$id',
    passport.authenticate('jtw', authOptions),
    async (req, res) => {

    }
);
