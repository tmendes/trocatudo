'use strict';

const passport = require('passport');
const router = require('express').Router();

const authOptions = { session: false };

const {
    productGet,
    productGetAll,
    productCreate,
    productGetMine,
    productPatch,
    productDelete
} = require('./../controllers/product-controller');

/**
 * It sends back all the products we have
 * ! GET /shares
 */
router.get(
    '/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await productGetAll();
        return res.status(data.code).json(data.json);
    }
);

/**
 * It creates a new product
 * ! POST /shares
 * @param {mongoose.Schema} req.user - User Model
 * @param {Object} req.body - New product JSON
 */
router.post(
    '/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await productCreate(req.user, req.body);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It sends back a specific user products
 * ! GET /shares/mine
 * @param {mongoose.Scham.Types.ObjectID} req.user.id - User Id
 */
router.get(
    '/mine',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await productGetMine(req.user.id);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It sends back the product info
 * ! GET /shares/productId
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Product Id
 */
router.get(
    '/:id',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await productGet(id);
        return res.status(data.code).json(data.json);
    }
);

/**
 * Update a user product info
 * ! PATCH /shares/productId
 * @param {mongoose.Schema} req.user - User Model
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Product Id
 * @param {Object} req.body - Updated product JSON
 */
router.patch(
    '/:id',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await productPatch(req.user, id, req.body);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It will delete a product
 * ! DELETE /shares/productId
 * @param {mongoose.Schema} req.user - User Model
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Product Id
 */
router.delete(
    '/:id',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await productDelete(req.user, id);
        return res.status(data.code).json(data.json);
    }
);

module.exports = router;
