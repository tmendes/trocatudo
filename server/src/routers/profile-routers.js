'use strict';

const passport = require('passport');
const router = require('express').Router();

const authOptions = { session: false };

const {
    profileGetUser,
    profileGet,
    profilePatch,
    profileDelete,
    profileSettingsGet,
    profileSettingsPatch
} = require('./../controllers/profile-controller');

router.get(
    '/:id',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await profileGetUser(id);
        return res.status(data.code).json(data.json);
    }
);

router.get(
    '/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await profileGet();
        return res.status(data.code).json(data.json);
    }
).patch(
    '/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await profilePatch(req.user, req.body);
        return res.status(data.code).json(data.json);
    }
).delete(
    '/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await profileDelete(req.user);
        return res.status(data.code).json(data.json);
    }
);

router.get(
    '/settings', passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await profileSettingsGet(req.user);
        return res.status(data.code).json(data.json);
    }
).patch(
    '/settings', passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await profileSettingsPatch(req.user, req.body);
        return res.status(data.code).json(data.json);
    }
);

module.exports = router;
