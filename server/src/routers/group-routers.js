'use strict';

const passport = require('passport');
const router = require('express').Router();

const {
    groupGetAll,
    groupCreate,
    groupDelete,
    groupGetMine,
    groupFilter,
    groupGet,
    groupPatch,
    groupMembersGetAll,
    groupMembersJoinPatch,
    groupMembersLeavePatch,
    groupMembersBlockPatch,
    groupMembersKickPatch,
    groupProductsGetAll,
    groupProductsGetMine,
    groupProductsAddPatch,
    groupProductsRemove
} = require('./../controllers/group-controller');

const authOptions = { session: false };

/**
 * It sends back all the groups we have
 * ! GET /group
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 */
router.get(
    '/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await groupGetAll();
        return res.status(data.code).json(data.json);
    }
);

/**
 * It creates a new group
 * ! POST /group
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {Object} req.body - New group json data
 */
router.post(
    '/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await groupCreate(req.user, req.body);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It deletes a valid group
 * ! DELETE /group/groupId
 * ? Tenho que achar todos os usuarios/produtos que tem esse grupo?
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Group Id
 * TODO: Verificar se eu sou um admin e se não tem mais admins
 */
router.delete(
    '/:id',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupDelete(req.user, id);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It sends back only the groups that belongs to the logged user
 * ! GET /group/mine
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 */
router.get(
    '/mine',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await groupGetMine(req.user.id);
        return res.status(data.code).json(data.json);
    }
);

/**
 * Find a group using filters
 * ! GET /group/search
 * @param {Object} req.body - JSON containing the filters
 */
router.get(
    '/group/search',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const data = await groupFilter(req.body);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It sends back a specific group
 * ! GET /group/groupId
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Group Id
 */
router.get(
    '/:id',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupGet(req.user.id, id);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It updates the attributes from a specific group
 * ! PATCH /group/groupId
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Group Id
 * @param {Object} req.body.data - A json containing the updated attributes
 */
router.patch(
    '/:id',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupPatch(req.user.id, id, req.body);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It sends back all the members from this specific group
 * ! GET /group/groupId/users/list
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Group Id
 */
router.get(
    '/:id/users/list',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupMembersGetAll(req.user.id, id);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It makes possible to the logged user to join an specific group
 * ! PATCH /group/groupId/users/join
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Group Id
 * @param {String} req.params.hash - Access Token
 */
router.patch(
    '/:id/users/join',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupMembersJoinPatch(req.user.id, id, req.body);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It makes possible to the logged user leave an specific group
 * ! PATCH /group/groupId/users/leave
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Group Id
 */
router.patch(
    '/:id/users/leave',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupMembersLeavePatch(req.user, id);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It makes possible to an admin to block an specific member
 * ! PATCH /group/groupId/users/block
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Group Id
 * @param {Object} req.body.data - A json containing user id we have to block
 * and if we should block or unblock the specific user
 */
router.patch(
    '/:id/users/block',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupMembersBlockPatch(req.user.id, id, req.body);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It makes possible to an admin to kick an user from a specific group
 * ! PATCH /group/groupId/users/kick
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Group Id
 * @param {Object} req.body.data - A json containing user id we have to kick
 */
router.patch(
    '/:id/users/kick',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupMembersKickPatch(req.user.id, id, req.body);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It sends back every product from this group
 * ! GET /group/groupId/products/list
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Group Id
 */
router.get(
    '/$id/products/list',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupProductsGetAll(req.user.id, id);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It sends back every product from this project that belongs to the logged user
 * ! GET /group/groupId/products/mine
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} id - Group Id
 */
router.get(
    '/:id/products/mine/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupProductsGetMine(req.user.id, id);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It adds a valid user product to a specific group
 * ! POST /group/groupId/products
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} req.params.id - Group Id
 * @param {Object} req.body.data - A json containing the product Id
 */
router.post(
    ':id/products/',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupProductsAddPatch(req.user, id, req.body);
        return res.status(data.code).json(data.json);
    }
);

/**
 * It removes a valid product from a specific group
 * ! DELETE /group/groupId/products
 * @param {mongoose.Schema.Types.ObjectID} req.user.id - User Id
 * @param {mongoose.Schema.Types.ObjectID} id - Group Id
 * @param {Object} req.body.data - A json containing the product Id
 */
router.delete(
    '/:id/products',
    passport.authenticate('jwt', authOptions),
    async (req, res) => {
        const { id } = req.params;
        const data = await groupProductsRemove(req.user, id, req.body);
        return res.status(data.code).json(data.json);
    }
);

module.exports = router;
