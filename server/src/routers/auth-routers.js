'use strict';

const router = require('express').Router();
const passport = require('passport');

const jrh = require('./../utils/jsonResponseHandler');

const {
    authLogin,
    authSignin
} = require('./../controllers/auth-controller');

/**
 * Just check if the token is valid or not
 * ! GET /auth/token/secret
 * @param {String} token - User token
 * @return {Object} - Auth Status message
 */
router.get(
    '/token/secret', passport.authenticate('jwt', { session: false }),
    (req, res) => res.json(jrh
        .success(`There you are ${req.user.username}! Logged in :)`))
);

/**
 * It generates a new Token if the user and password is valid
 * ! POST /auth/token/login
 * @param {String} req.body.name - Login Info
 * @param {String} req.body.password - User Password
 * @return {Object} - JSON Token
 */
router.post('/token/login', async (req, res) => {
    const data = await authLogin(
        req.body.username,
        req.body.password
    );
    return res.status(data.code).json(data.json);
});


/**
 * It creates a new user into the system
 * ! GET /auth/token/signup
 * @param {String} req.body.name - Login Info
 * @param {String} req.body.password - User Password
 * @return {Object} - JSON Token
 */
router.post('/token/signup', async (req, res) => {
    const data = await authSignin(
        req.body.username,
        req.body.email,
        req.body.password
    );
    return res.status(data.code).json(data.json);
});


module.exports = router;
