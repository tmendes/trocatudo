'use strict';

const env = process.env.NODE_ENV || 'development';
const configJSON = require('./server-config.json');

console.debug(`Server running at ${env} mode`);

if (env === 'development') {
    const config = configJSON[env];
    const configKeys = Object.keys(config);

    configKeys.forEach(key => {
        console.debug(`Setting ${key} key as ${config[key]}`);
        process.env[key] = config[key];
    });
}
