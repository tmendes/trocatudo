# Resumo

(Faça um resumo do problema encontrado)

# Passos para reproduzir

(Como nós podemos reproduzir este problema? Lembre-se que uma descrição clara e
detalhada é muito importante para que possamos trabalhar em uma solução)

# Qual é o comportamento atual do problema?

(O que acontece?)

# Qual é o comportamento esperado?

(Qual o resultado que você esperava encontrar em um comportamento normal (sem
problemas))

# Fotos da tela e logs relevantes

(Cole aqui os logs ou fotos da tela relevantes ao problema. Lembrando que para
colar logs você precisa colocar todo o texto entre (```))

# Como você imagina a correção deste problema?

(Se for possível, coloque a linha de código que você acredita ser responsável
por gerar o problema reportado)
