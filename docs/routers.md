# ROUTES

## AUTH

 * GET    /auth/token/secret
 * POST   /auth/token/generate
 * GET    /auth/token/signup

## GROUP

 * GET    /group
 * GET    /group/mine
 * POST   /group/search
 * GET    /group/groupId
 * POST   /group/new
 * DELETE /group/delete/groupId
 * GET    /group/users/groupId
 * PATCH  /group/edit/groupId
 * PATCH  /group/join/groupId
 * PATCH  /group/leave/groupId
 * PATCH  /group/user/block/groupId
 * PATCH  /group/user/kick/groupId
 * GET    /group/products/groupId
 * GET    /group/products/mine/groupId
 * PUT    /group/products/add/groupId
 * PATCH  /group/products/remove/groupId

## PRODUCT

 * GET    /product
 * GET    /product/mine
 * POST   /product/new
 * GET    /product/productId
 * PATCH  /product/productId
 * DELETE /product/delete/productId
